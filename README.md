# Hypergeometric Functions

This formulary contains a number of useful relations of various generalised hypergeometric functions collected (or in some cases established) by the editor over the years. The term 'editor' implies the hope that there will be more contributing authors in the future.

The current version of the formulary is contained in the file `Hypergeom.pdf`.

If you want to add or change anything you can do this in the LaTeX code `Hypergeom.tex` and send a pull request. You can also just raise an issue like "Please add formula (...) from reference [...]" and I will include that if it is appropriate.
